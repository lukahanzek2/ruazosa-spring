package hr.fer.ruazosa.timesheet.entity;

import java.util.ArrayList;
import java.util.List;

public class ProjectHours {
    private Long projectId;
    private Integer hours;

    public ProjectHours(Long projectId, Integer hours) {
        this.projectId = projectId;
        this.hours = hours;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public Long getProjectId() {
        return projectId;
    }

    public Integer getHours() {
        return hours;
    }

    public static List<ProjectHours> stringHours2dToObject(String[][] raw){
        List<ProjectHours> list = new ArrayList<ProjectHours>();

        for(int i = 0; i < raw.length; i++){
            list.add(new ProjectHours(Long.parseLong(raw[i][0]), Integer.parseInt(raw[i][1])));
        }
        return list;
    }
}
