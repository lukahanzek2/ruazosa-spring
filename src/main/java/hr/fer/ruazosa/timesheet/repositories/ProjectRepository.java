package hr.fer.ruazosa.timesheet.repositories;

import hr.fer.ruazosa.timesheet.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {

	@Query(value = "SELECT * FROM projects JOIN users_projects ON projects.project_id = users_projects.project_id "
			+ "WHERE users_projects.user_id = :userId", nativeQuery = true)
	List<Project> findAllByUserId(@Param("userId") Long userId);

	Project findById(Long projectId);


	@Query(value =
			"SELECT users_projects.project_id, ISNULL(SUM(hours), 0) AS \"hours\" FROM users " +
					"JOIN users_projects ON users.user_id = users_projects.user_id " +
					"LEFT JOIN " +
					"(SELECT projects_tasks.project_id as project_id2, projects_tasks.task_id as task_id2, users_tasks.user_id as user_id2 FROM projects_tasks " +
						"JOIN users_tasks ON projects_tasks.task_id = users_tasks.task_id) on users.user_id = user_id2 and users_projects.project_id = project_id2 " +
					"LEFT JOIN tasks ON task_id2 = tasks.task_id " +
					"WHERE users.username = :user_name " +
					"GROUP BY(users.user_id, users_projects.project_id)", nativeQuery = true)
	String[][] projectsHoursForUser(@Param("user_name") String userName);
}
