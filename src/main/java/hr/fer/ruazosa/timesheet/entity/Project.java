package hr.fer.ruazosa.timesheet.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="projects")
public class Project {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "project_id")
    private Long id;
	@Column(name = "project_name")
	private String projectName;
	@Transient
	private int hours;

	@OneToMany(cascade=CascadeType.PERSIST)
	@JoinTable(
			name = "projects_tasks",
			joinColumns = @JoinColumn(name = "project_id"),
			inverseJoinColumns = @JoinColumn(name = "task_id")
	)
	private List<Task> tasks = new ArrayList<>();
	public void addTask(Task task) {
		tasks.add(task);
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getProjectName() {
		return projectName;
	}
	
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public void setHours(Integer hours){
		this.hours = hours;
	}

	public Integer getHours(){
		return this.hours;
	}
}
