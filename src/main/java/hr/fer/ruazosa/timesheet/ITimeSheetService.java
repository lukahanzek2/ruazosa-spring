package hr.fer.ruazosa.timesheet;

import hr.fer.ruazosa.timesheet.entity.Project;
import hr.fer.ruazosa.timesheet.entity.ProjectHours;
import hr.fer.ruazosa.timesheet.entity.Task;
import hr.fer.ruazosa.timesheet.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface ITimeSheetService extends UserDetailsService {
    User registerUser(User user);
    boolean checkUsernameUnique(User user);
    //User loginUser(User user);
    List<Project> listProjects(String userName);
    void makeProject(Project project);
    void addProject(Long projectId, String userName);
    void addTask(String userName, Long projectId, Task task);
    List<Task> listTasks(Long projectId, String username);
    List<ProjectHours> projectsHoursForUser(String userName);
    void editTask(Task task, Long taskId);
    User getUserByUsername(String username);
}
