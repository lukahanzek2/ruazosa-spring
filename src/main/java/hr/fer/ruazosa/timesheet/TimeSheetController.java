package hr.fer.ruazosa.timesheet;


import hr.fer.ruazosa.timesheet.entity.Project;
import hr.fer.ruazosa.timesheet.entity.ProjectHours;
import hr.fer.ruazosa.timesheet.entity.Task;
import hr.fer.ruazosa.timesheet.entity.User;
import hr.fer.ruazosa.timesheet.jwt.JwtRequest;
import hr.fer.ruazosa.timesheet.jwt.JwtResponse;
import hr.fer.ruazosa.timesheet.jwt.JwtUtils;
import hr.fer.ruazosa.timesheet.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
public class TimeSheetController {

    @Autowired(required = true)
    private AuthenticationManager authenticationManager;

    @Autowired
    private ITimeSheetService timesheetService;

    @Autowired
    private JwtUtils jwtUtils;

    @GetMapping("/helloWorld")
    public String helloWorld(){
        return "HelloWorld";
    }

    @PostMapping("/registerUser")
    public ResponseEntity<Object> registerUser(@RequestBody User user) {
        // validation
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<User>> violations = validator.validate(user);

        Map<String, Object> body = new LinkedHashMap<>();
        for (ConstraintViolation<User> violation : violations) {
            body.put(violation.getPropertyPath().toString(), violation.getMessage());
        }
        if (!body.isEmpty()) {
            return new ResponseEntity<Object>(body, HttpStatus.NOT_ACCEPTABLE);
        }

        boolean userExists = timesheetService.registerUser(user) == null ? true : false;
        if(userExists){
            return new ResponseEntity<Object>("User with same username already exists", HttpStatus.NOT_ACCEPTABLE);
        }
        return new ResponseEntity<Object>(user, HttpStatus.OK);
    }

    /*
    @PostMapping("/loginUser")
    public ResponseEntity<Object> loginUser(@RequestBody(required = false) User user) {
        // validation
        if (user == null) {
            Map<String, Object> body = new LinkedHashMap<>();
            body.put("error", "no user JSON object in body");
            return new ResponseEntity<Object>(body, HttpStatus.NOT_ACCEPTABLE);
        }
        else if (user.getUsername().isEmpty() || user.getPassword().isEmpty()) {
            Map<String, Object> body = new LinkedHashMap<>();
            body.put("error", "username or password parameters are empty");
            return new ResponseEntity<Object>(body, HttpStatus.NOT_ACCEPTABLE);
        }
        else {
            User loggedUser = timesheetService.loginUser(user);
            if (loggedUser != null) {
                return new ResponseEntity<Object>(loggedUser, HttpStatus.OK);
            }
            else {
                Map<String, Object> body = new LinkedHashMap<>();
                body.put("error", "no user found");
                return new ResponseEntity<Object>(body, HttpStatus.NOT_FOUND);
            }
        }

    }
    */
    
    @PostMapping("/makeProject")
    public void makeProject(@RequestBody Project project) {
    	timesheetService.makeProject(project);
    }
    
    @PostMapping("/addProject/{projectId}/{userName}")
    public void addProject(@PathVariable Long projectId, @PathVariable String userName) {
    	timesheetService.addProject(projectId, userName);
    }
    
    @GetMapping("projects/{userName}")
    public ResponseEntity<List<Project>> listProjects(@PathVariable String userName) {
        List<Project> projects = timesheetService.listProjects(userName);

    	return ResponseEntity.ok(projects);
    }

    @PostMapping("addTask/{userName}/{projectId}")
    public ResponseEntity<Object> addTask(@RequestBody Task task, @PathVariable String userName, @PathVariable Long projectId){
        System.out.println(task);
        timesheetService.addTask(userName, projectId, task);
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("error", "");

        return ResponseEntity.ok(body);
    }

    @GetMapping("tasks/{projectId}/{userName}")
    public ResponseEntity<List<Task>> listTasks(@PathVariable String userName, @PathVariable Long projectId) {
        return ResponseEntity.ok(timesheetService.listTasks(projectId, userName));
    }

    @GetMapping("hours/{userName}")
    public ResponseEntity<List<ProjectHours>> hours(@PathVariable String userName){
        return ResponseEntity.ok(timesheetService.projectsHoursForUser(userName));
    }
    
    @PutMapping("editTask/{taskId}")
    public ResponseEntity<Object> editTask(@RequestBody Task task, @PathVariable Long taskId){
        timesheetService.editTask(task, taskId);
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("error", "");

        return ResponseEntity.ok(body);
    }

    @PostMapping("/authenticate")
    public ResponseEntity<Object> createAuthenticationToken(@RequestBody JwtRequest jwtRequest) throws Exception {

        authenticate(jwtRequest.getUsername(), jwtRequest.getPassword());

        UserDetails userDetails = timesheetService.loadUserByUsername(jwtRequest.getUsername());
        final String token = jwtUtils.generateToken(userDetails);

        User tempUser = timesheetService.getUserByUsername(userDetails.getUsername());
        JwtResponse response = new JwtResponse(token, tempUser);
        return ResponseEntity.ok(response);
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
