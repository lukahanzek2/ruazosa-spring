package hr.fer.ruazosa.timesheet;

import hr.fer.ruazosa.timesheet.entity.Project;
import hr.fer.ruazosa.timesheet.entity.ProjectHours;
import hr.fer.ruazosa.timesheet.entity.Task;
import hr.fer.ruazosa.timesheet.entity.User;
import hr.fer.ruazosa.timesheet.repositories.ProjectRepository;
import hr.fer.ruazosa.timesheet.repositories.TaskRepository;
import hr.fer.ruazosa.timesheet.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TimeSheetService implements ITimeSheetService {

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public User registerUser(User user) {
        if(checkUsernameUnique(user) == false){
            return null;
        }
        return userRepository.save(user);
    }

    @Override
    public boolean checkUsernameUnique(User user) {
        List<User> usersWithSameUsername = userRepository.findByUserName(user.getUsername());
        return usersWithSameUsername.isEmpty() ? true : false;
    }
    /*
    @Override
    public User loginUser(User user) {
        List<User> loggedUserList = userRepository.findByUserNameAndPassword(user.getUsername(), user.getPassword());
        if (loggedUserList.isEmpty()) {
            return null;
        }
        return userRepository.findByUserNameAndPassword(user.getUsername(), user.getPassword()).get(0);
    }
    */

	@Override
	public List<Project> listProjects(String userName) {
		User user = userRepository.findByUserName(userName).get(0);
		List<Project> projects = projectRepository.findAllByUserId(user.getId());
        String[][] raw = projectRepository.projectsHoursForUser(userName);
        List<ProjectHours> projectHours = ProjectHours.stringHours2dToObject(raw);
        for(int i = 0; i < projects.size(); i++){
            for(int j = 0; j < projectHours.size(); j++){
                if(projects.get(i).getId() == projectHours.get(j).getProjectId()){
                    projects.get(i).setHours(projectHours.get(j).getHours());
                }
            }
        }
        if(projects.isEmpty()){
            projects.add(null);
        }
        System.out.println("Returning: " + projects.toString());
        return projects;
	}

	@Override
	public void addProject(Long projectId, String userName) {
		User user = userRepository.findByUserName(userName).get(0);
		Project project = projectRepository.findById(projectId);
		user.addProject(project);
		userRepository.save(user);
	}

	@Override
	public void makeProject(Project project) {
		projectRepository.save(project);
	}

    @Override
    public void addTask(String userName, Long projectId, Task task) {
        User updatedUser = userRepository.findByUserName(userName).get(0);
        Project updatedProject = projectRepository.findById(projectId);
        taskRepository.save(task);
        updatedUser.addTask(task);
        updatedProject.addTask(task);
        userRepository.save(updatedUser);
        projectRepository.save(updatedProject);
    }

    @Override
    public List<Task> listTasks(Long projectId, String username){
        List<Task> tasks = taskRepository.findAllByUserNameProjectId(projectId, username);
        System.out.println("Returning: " + tasks.toString());
        return tasks;
    }

    @Override
    public List<ProjectHours> projectsHoursForUser(String userName){
        String[][] raw = projectRepository.projectsHoursForUser(userName);
        return ProjectHours.stringHours2dToObject(raw);
    }

	@Override
	public void editTask(Task task, Long taskId) {
		Task updatedTask = taskRepository.findById(taskId);
		updatedTask.setTaskName(task.getTaskName());
		updatedTask.setTaskDescription(task.getTaskDescription());
		updatedTask.setDate(task.getDate());
		updatedTask.setHours(task.getHours());
		taskRepository.save(updatedTask);
	}

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        List<User> users = userRepository.findByUserName(username);

        if (users.isEmpty()) {
            return null;
        }
        User user = users.get(0);
        org.springframework.security.core.userdetails.User userDetails =
                new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), new ArrayList<>());
        return userDetails;
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.findByUserName(username).get(0);
    }
}
