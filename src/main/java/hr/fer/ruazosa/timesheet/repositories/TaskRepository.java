package hr.fer.ruazosa.timesheet.repositories;

import hr.fer.ruazosa.timesheet.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> {
    @Query(value = "SELECT tasks.task_id, tasks.task_name, tasks.task_description, tasks.date, tasks.hours FROM users " +
                    "JOIN users_projects ON users.user_id = users_projects.user_id " +
                    "LEFT JOIN " +
                    "(SELECT projects_tasks.project_id as project_id2, projects_tasks.task_id as task_id2, users_tasks.user_id as user_id2 FROM projects_tasks " +
                    "JOIN users_tasks ON projects_tasks.task_id = users_tasks.task_id) on users.user_id = user_id2 and users_projects.project_id = project_id2 " +
                    "LEFT JOIN tasks ON task_id2 = tasks.task_id " +
                    "WHERE users.username = :userName AND users_projects.project_id = :projectId", nativeQuery = true)
    List<Task> findAllByUserNameProjectId(@Param("projectId") Long projectId, @Param("userName") String userName);
    
    Task findById(Long taskId);
}
